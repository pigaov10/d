# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from department import Department


class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=60,
                            blank=False)
    email = models.EmailField()
    departament = models.ForeignKey('Department',
                                    models.DO_NOTHING,
                                    blank=True,
                                    null=True)

    def __str__(self):
        return unicode(self.name).encode('utf-8', 'ignore')

    class Meta:
        verbose_name_plural = 'employees'
        managed = False
        db_table = 'employee'
